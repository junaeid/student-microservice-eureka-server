# Eureka Server of Student Microservice

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
         <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li>
        <a href="#step-by-step-process-of-project-creation">Step by Step Process of Project Creation</a>
        <ul>
        <li><a href="#"></a></li>
         <li><a href="#"></a></li>
        </ul>
    </li>
    <li>
        <a href="#conclusion">Conclusion</a>
    </li>
  </ol>
</details>

### About The Project

This project consists 4 parts where the Eureka server build the connection between applications
1. [Student Microservice Eureka Server](https://gitlab.com/junaeid/student-microservice-eureka-server.git)
2. [Student Microservice Api Gateway](https://gitlab.com/junaeid/student-microservice-api-gateway)
3. [Student Microservice](https://gitlab.com/junaeid/student-microservice)
4. [Address Microservice](https://gitlab.com/junaeid/address-microservice)



### Built With

- Springboot(2.7.2)
- Cloud Netflix – Eureka Server 2021.0.3

### Prerequisites

- IntelliJ IDEA(recommended)
- Java11/OpenJdk11
- Gradle

### Creation and Configuration of Eureka Server
1. Add **Eureka Server** dependency in the build.gradle file
 ```gradle
implementation 'org.springframework.cloud:spring-cloud-starter-netflix-eureka-server'
```
2. In application.properties add server port and server configuration
```.properties
# Recommended Port
server.port=8761 

# Exclude eureka server from registering with itself
spring.application.name=eureka-server 
eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false
```
3. In the application file where the main method situated; add the annotation @EnableEurekaServer
```java
@EnableEurekaServer
@SpringBootApplication
public class StudentServiceEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentServiceEurekaServerApplication.class, args);
    }

}

```
### Configuring Eureka Client

1. Add Eureka client dependency to the build.gradle file 
```properties
implementation 'org.springframework.cloud:spring-cloud-starter-netflix-eureka-client'
```
2. Enable Eureka client to the microservices by adding @EnableEurekaClient annotation to the main class
```java
@EnableEurekaClient
@SpringBootApplication
public class StudentMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentMicroserviceApplication.class, args);
    }

}

```
3. Set the Eureka Server Url in the application.properties file
```properties
eureka.client.service-url.defaultZone=http://localhost:8761/eureka
```
4. (Optional) Use the feignClient with the value that is defined in the eureka server instead of the URL
```java
@FeignClient(value = "API-GATEWAY")
public interface AddressFeignClient {

    @GetMapping("address-service/api/v1/address/{id}")
    public AddressResponse getById(@PathVariable("id") Long id);
}

```
**Note**: Name should be in the same format as it is in eureka server. 

#### Run and test

### Conclusion




